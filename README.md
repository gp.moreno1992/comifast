# ComiFast
Autor: Paúl Moreno
Proyecto final empleando el aprendizaje obtenido en cloud computing.
Para clonar el proyecto:
git clone https://gitlab.com/gp.moreno1992/comifast.git.
La imagen a usar es php 7, se encuentra el consumo desde el archivo Dockefile.
Se realiza una conexión a mysql8, se encuentra configurada en el dokcer-compose.yml.
La estructura de base de datos se encuetra en el archivo code.sql, eso debe crear en su motor de base de datos.
Para ejecutar: docker-compose up.
Verificar en el browser: 1092.168.100.250:8000
