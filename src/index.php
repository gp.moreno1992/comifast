<!DOCTYPE html>
<html lang="en">

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <!-- Bootstrap CSS -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
      crossorigin="anonymous"
    />

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Estudiantes</title>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>Clientes</h2>
        </div>
      </div>
      <hr />
      <a href="../views/nuevo.php" type="button" class="btn btn-primary">Crear nuevo</a>
    
    <legend>Registros</legend>
  <?php
 $conn = new mysqli("database", "root", ".sweetpwd.", "my_db");
   // Check connection
   if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
   }

        $sql = "SELECT * FROM clientes";
        $result = $conn->query($sql);
        // output data of each row
        if ($result->num_rows > 0) {
 ?>
   <table class="table table-striped">
      <thead>
        <tr>
            <td>Id</td>
            <td>Cédula</td>
            <td>Nombre</td>
            <td>Edad</td>
            <td>Telefono</td>
        </tr>
      </thead>
      <tbody>
<?php
        while($row = $result->fetch_assoc()) {
?>     
   <tr>
          <td><?php echo $row["id"] ?></td>
          <td><?php echo $row["cedula"] ?></td>
          <td><?php echo $row["nombre"] ?></td>
          <td><?php echo $row["edad"] ?></td>
          <td><?php echo $row["telefono"] ?></td>
          <td>
            <a href="">Editar</a>
            <a type="button" class="btn btn-danger" href="../views/eliminar.php?id=<?php echo $row["id"] ?>">Eliminar</a>
          </td>
        </tr>
        <?php }  $conn->close();?>
        
      </tbody>
    </table>
	<?php }else{echo "no result";}?>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
      src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
      integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
      integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
      crossorigin="anonymous"
    ></script>
  </body>
</html>

